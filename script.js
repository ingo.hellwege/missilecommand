/* class settings */
class Settings{
  /* constructor */
  constructor(){
    // setup defaults
    this.setupDefaults();
  }


  /* setup defaults */
  setupDefaults(){
    // defaults
    this.run=false;                      // game status
    this.con=false;                      // game level (new/continue)
    this.brk=false;                      // game pause
    this.blk=false;                      // block screen update
    this.winw=0;                         // screen width
    this.winh=0;                         // screen height
    this.nln=String.fromCharCode(10);    // line break
    this.mblthr=960;                     // mobile threshold
    this.plrpts=0;                       // score
    this.plrlvl=0;                       // level
    this.shtlst=[];                      // shot list
    this.shtaud='';                      // shot audio
    this.shtspd=6;                       // shot speed
    this.shtlow=100;                     // shot now lower this.SET.winh-this
    this.enmshttmr=2000;                 // enemy shot every n ms
    this.enmshtamtdef=50                 // enemy shot default amount
    this.enmshtamt=0;                    // enemy shot amount
    this.baslst=[];                      // base list
    this.basamo=20;                      // base default ammo
    this.ctyamtdef=6;                    // city defaultamount
    this.ctyamt=0;                       // city amount
    this.ctylst=[];                      // city list
    this.expaud='';                      // explosion audio
    this.explst=[];                      // explosion list
    this.expraddef=120;                  // explosion default radius
    this.exprad=0;                       // explosion radius
    this.expspd=6;                       // explosion speed
    this.btnaud='';                      // button audio
    this.hrnaud='';                      // horn audio
  }
}

/* class Tools */
class Tools{
  /* constructor */
  constructor(set){
    this.SET=set;
  }


  /* add class to element */
  addClassToElementById(elmid,elmcls){
    var cls=this.getClassStringForElementById(elmid);
    if(cls.includes(elmcls)==false){
      var newcls=cls.concat(' '+elmcls);
      document.getElementById(elmid).className=newcls;
    }
  }

  /* remove class from element */
  removeClassFromElementById(elmid,elmcls){
    var cls=this.getClassStringForElementById(elmid);
    var newcls=cls.split(elmcls).join('');
    document.getElementById(elmid).className=newcls;
  }

  /* get the string with class names for element */
  getClassStringForElementById(elmid){
    return document.getElementById(elmid).className;
  }

  /* get inner html for element by id */
  getElementInnerHtmlById(elmid){
    return document.getElementById(elmid).innerHTML;
  }

  /* set inner html for element by id */
  setElementInnerHtmlById(elmid,str){
    document.getElementById(elmid).innerHTML=str;
  }

  /* get visibility for element (class with 'show') */
  isElementVisibleById(elmid){
    return this.getClassStringForElementById(elmid).includes('show')==true;
  }

  /* hide cover layer */
  hideCover(){
    this.addClassToElementById('cover1','cover1hide');
    this.addClassToElementById('cover2','cover2hide');
  }

  /* format number with leading zero */
  numFormat(num,size){
    var str="000000000"+num;
    return str.substr(str.length-size);
  }

  /* convert degree to radiant */
  convertDegToRad(deg){
    var rad=deg*(Math.PI/180.0);
    return rad;
  }

  /* get speeds (x,y 0-1) for direction angle */
  getSpeedsForAngle(deg){
    // correction
    deg-=180;
    //get speeds for angle
    var rad=this.convertDegToRad(deg);
    var spx=Math.cos(rad);
    var spy=Math.sin(rad);
    // result array
    return [spx,spy];
  }

  /* get a random hex color string */
  getRandomHexColorString(){
    return '#'+(Math.random()*0xFFFFFF<<0).toString(16).padStart(6,'0');
  }

  /* cleanup list (which + check flag) */
  cleanUpList(lst,flg){
    // create new list
    var nlst=[];
    lst.forEach((data)=>{
      if(data[flg]==1){
        nlst.push(data);
      }
    });
    // return new list
    return nlst;
  }

  /* remove item from array by key */
  removeItemFromArrayByKey(key,arr){
    arr.splice(key,1);
    return arr;
  }

  /* add item to array */
  addItemToArray(item,arr){
    arr.push(item);
    return arr;
  }

  /* play sound */
  playSound(snd){
    snd.cloneNode(true).play();
  }
}

/* class Sound */
class Sound{
  /* constructor */
  constructor(){
    this.setupAudio();
  }


  /* setup game audio files */
  setupAudio(){
    this.shtaud=new Audio('shot.wav');
    this.shtaud.volume=0.25;
    this.expaud=new Audio('explosion.wav');
    this.expaud.volume=0.5;
    this.btnaud=new Audio('click.wav');
    this.btnaud.volume=0.75;
    this.hrnaud=new Audio('horn.wav');
    this.hrnaud.volume=0.25;
  }
}

/* class Game */
class Game{
  /* constructor */
  constructor(){
    // objects
    this.SET=new Settings();
    this.TLS=new Tools(this.SET);
    this.SND=new Sound();
    // event listener
    addEventListener('click',this);
    addEventListener('keydown',this);
    addEventListener('resize',this);
  }


  /* get amount of assets (bases and cities) */
  hasPlayerAssets(){
    // city amount > 0
    if(this.getCitiesAmount()>0){return true;}
    return false;
  }

  /* add shots to list */
  addShotToList(which){
    // default
    var item=[];
    // which list
    switch(which){
      case 'shtlst':
        // mouse click position
        var mx=window.event.clientX;
        var my=window.event.clientY;
        // correction for low click
        if(my>this.SET.winh-this.SET.shtlow){my=this.SET.winh-this.SET.shtlow;}
        // get nearest base (with ammo)
        var bobj=this.getNearestBaseToPos(mx);
        // if object was found
        if(bobj!=null){
          // get key
          var bkey=bobj.getAttribute('attr-key');
          // shot position
          var px=bobj.offsetLeft+((bobj.offsetWidth/2)+5);
          var py=this.SET.winh-bobj.offsetHeight-(bobj.offsetHeight/2)-5;
          // calculate distance, angle, speed
          var dist=Math.sqrt(Math.pow(px-mx,2)+Math.pow(py-my,2));
          var deg=Math.atan2(py-my,px-mx)*(180/Math.PI);
          var spd=this.TLS.getSpeedsForAngle(deg);
          var sx=spd[0];
          var sy=spd[1];
          // create item (px,py,sx,sy,type,range,alive)
          item=[px,py,sx*this.SET.shtspd,sy*this.SET.shtspd,'sht',dist/this.SET.shtspd,1];
          // reduce base ammo and update
          var data=this.SET.baslst[bkey];
          data[1]--;
          this.SET.baslst[bkey]=data;
        }
        break;
      case 'enmshtlst':
        // position and speed
        var px=(Math.random()*(this.SET.winw-160))+80;
        var sx=Math.random()-0.5;
        var sy=1.25+(this.SET.plrlvl/5);
        // faster if there is no ammo left
        if(this.getBasesTotalAmmoAmount()==0){sy=8;}
        // create item (px,py,sx,sy,type,range,alive)
        item=[px,0,sx,sy,'enm',0,1];
        break;
    }
    // add to list
    if(item.length>0){
      // slower for mobile
      if(this.SET.winw<=this.SET.mblthr){
        item[2]*=0.75;   // 25% slower
        item[3]*=0.75;   // 25% slower
        item[5]*=1.25;   // 25% more range
      }
      this.SET.shtlst=this.TLS.addItemToArray(item,this.SET.shtlst);
    }
  }

  /* add enemy shot with timer */
  addEnemyShotByTimer(){
    // add shot if game is this.SET.running
    if(this.SET.run==true&&this.SET.brk==false){
      // add shot
      this.addShotToList('enmshtlst');
      // count down
      this.SET.enmshtamt--;
      if(this.SET.enmshtamt<0){this.SET.enmshtamt=0;}
    }
    // until more shots needed
    if(this.SET.enmshtamt>0){
      // get faster every level wih a bit of randomness
      var tmr=this.SET.enmshttmr-((this.SET.plrlvl*50)+Math.random()*750);
      if(tmr<100){tmr=100;}
      // faster if there is no ammo left
      if(this.getBasesTotalAmmoAmount()==0){tmr*=0.25;}
      // do it again
      setTimeout(()=>{this.addEnemyShotByTimer();},parseInt(tmr));
    }
  }

  /* get amount of visible shots */
  getShotsAmount(){
    return this.SET.shtlst.length;
  }

  /* set shot dead for given key */
  setShotDeadForKey(skey){
    this.SET.shtlst[skey][6]=0;
  }

  /* update shot data */
  updateShotData(){
    var key=0;
    this.SET.shtlst.forEach((data)=>{
      if(this.SET.shtlst.hasOwnProperty(key)){
        // update position
        data[0]+=data[2];
        data[1]+=data[3];
        // remove if off screen | work with data
        if(data[0]<0||data[0]>this.SET.winw||data[1]<0||data[1]>this.SET.winh){
          this.setShotDeadForKey(key);
        }else{
          switch(data[4]){
            case 'sht':
              data[5]--;
              if(data[5]<=0){
                this.TLS.playSound(this.SND.expaud);
                this.setShotDeadForKey(key);
                this.addExplosion(data[0],data[1],'m');
              }
              break;
            case 'enm':
              // nothing by now
              break;
          }
        }
        // update data
        this.SET.shtlst[key]=data;
      }
      key++;
    });
  }

  /* move shots */
  moveShots(){
    // only if game is active
    if(this.SET.run==true){
      // move shots
      this.updateShotData();
    }
  }

  /* draw shots */
  drawShots(){
    // new html
    var html='';
    var key=0;
    this.SET.shtlst.forEach((data)=>{
      var cls=(data[4]=='sht')?'shot':'rocket';
      html+='    <div class="'+cls+'" style="top:'+parseInt(data[1])+'px;left:'+parseInt(data[0])+'px;" attr-key="'+key+'" attr-type="'+data[4]+'"></div>'+this.SET.nln;
      key++;
    });
    // set html
    this.TLS.setElementInnerHtmlById('shots',html);
  }



  /* draw explosions */
  drawExplosions(){
    // new html
    var html='';
    var key=0;
    this.SET.explst.forEach((data)=>{
      // size and position
      var wid=data[3]-data[2];
      var hei=data[3]-data[2];
      var px=data[0]-(wid/2);
      var py=data[1]-(hei/2);
      // html;
      html+='    <div class="explo" style="width:'+parseInt(wid)+'px;height:'+parseInt(hei)+'px;top:'+parseInt(py)+'px;left:'+parseInt(px)+'px;" attr-key="'+key+'"></div>'+this.SET.nln;
      // animate size by counter (inverted way)
      data[2]-=this.SET.expspd;
      // update data or not
      var updt=true;
      // remove if max size (counter < 04
      if(data[2]<0){
        this.setExplosionDoneForKey(key);
        updt=false;
      }
      // update data
      if(updt==true){this.SET.explst[key]=data;}
      key++;
    });
    // set html
    this.TLS.setElementInnerHtmlById('explos',html);
  }

  /* random color for explosions */
  setRandomColorForExplosions(){
    var explst = document.querySelectorAll('div.explo');
    if(explst.length>0){
      explst.forEach((item)=>{
        item.style.backgroundColor=this.TLS.getRandomHexColorString();
      });
    }
  }

  /* set explosion dead for given key */
  setExplosionDoneForKey(ekey){
    this.SET.explst[ekey][4]=0;
  }

  /* add explosion to list */
  addExplosion(px,py,type){
    // which size
    var size=0;
    switch(type){
      case 's':
        size=this.SET.exprad/3;
        break;
      case 'm':
        size=this.SET.exprad;
        break;
      case 'x':
        size=this.SET.exprad*3;
        break;
    }
    // create item (px,py,count,size,alive)
    var item=[px,py,size,size,1];
    // add to list
    this.SET.explst=this.TLS.addItemToArray(item,this.SET.explst);
  }



  /* count points and return total */
  setPointResultList(){
    // default
    var amt,val,total=0;
    // new html
    var html='';
    amt=this.TLS.numFormat(this.getBasesAmount(),2);
    val=amt*100;
    total+=val;
    html+='  base: '+amt+'x100='+this.TLS.numFormat(val,4)+'<br/>'+this.SET.nln;
    amt=this.TLS.numFormat(this.getCitiesAmount(),2);
    val=amt*50;
    total+=val;
    html+='  city: '+amt+'x050='+this.TLS.numFormat(val,4)+'<br/>'+this.SET.nln;
    amt=this.getBasesTotalAmmoAmount();
    val=amt*10;
    total+=val;
    html+='  ammo: '+amt+'x010='+this.TLS.numFormat(val,4)+'<br/>'+this.SET.nln;
    html+='  ---------------------<br/>'+this.SET.nln;
    html+='           sum='+this.TLS.numFormat(total,6)+'<br/>'+this.SET.nln;
    html+='        level: x'+this.TLS.numFormat(this.SET.plrlvl,3)+'<br/>'+this.SET.nln;
    html+='  =================<br/>'+this.SET.nln;
    total*=this.SET.plrlvl;
    html+='         total='+this.TLS.numFormat(total,6)+'<br/>'+this.SET.nln;
    // set content
    this.TLS.setElementInnerHtmlById('pointslist',html);
    // return total
    return total;
  }

  /* add points to player points */
  addPoints(pts){
    this.SET.plrpts=parseInt(this.TLS.getElementInnerHtmlById('points'))+pts;
  }

  /* draw points */
  drawPoints(){
    this.TLS.setElementInnerHtmlById('points',this.TLS.numFormat(this.SET.plrpts,8));
  }



  /* level up */
  addLevel(){
    this.SET.plrlvl++;
  }

  /* draw level */
  drawLevel(){
    var html=this.TLS.numFormat(this.SET.plrlvl,3)+':'+this.TLS.numFormat(this.SET.enmshtamt,2);
    this.TLS.setElementInnerHtmlById('level',html);
  }

  /* addon value depending on level */
  getLevelAddonValue(){
    return this.SET.plrlvl*2;
  }



  /* draw cities */
  drawCities(){
    // new html
    var html='';
    var key=0;
    this.SET.ctylst.forEach((data)=>{
      // visible or not
      var cls='';
      if(data[2]==0){cls=' disabled';}
      html+='    <div class="city'+cls+'" style="left:'+parseInt(data[0])+'px;" attr-key="'+key+'"></div>'+this.SET.nln;
      key++;
    });
    // set html
    this.TLS.setElementInnerHtmlById('cities',html);
  }

  /* get amount of visible cities */
  getCitiesAmount(){
    return parseInt(document.querySelectorAll('#cities>div:not(.disabled)').length);
  }



  /* draw bases */
  drawBases(){
    // new html
    var html='';
    var key=0;
    this.SET.baslst.forEach((data)=>{
      // visible or not
      var cls='';
      if(data[2]==0){cls=' disabled';}
      html+='    <div class="base'+cls+'" style="left:'+parseInt(data[0])+'px;" attr-key="'+key+'">'+data[1]+'</div>'+this.SET.nln;
      key++;
    });
    // set html
    this.TLS.setElementInnerHtmlById('bases',html);
  }

  /* get amount of visible bases */
  getBasesAmount(){
    return parseInt(document.querySelectorAll('#bases>div:not(.disabled)').length);
  }

  /* refill ammo for all bases */
  refillBasesAmmo(){
    var key=0;
    // every base gets the calculated amount
    this.SET.baslst.forEach((data)=>{
      data[1]=this.SET.basamo+this.getLevelAddonValue();   // ammo
      data[2]=1;                                           // alive
      this.SET.baslst[key]=data;
      key++;
    });
    // distribute ammo to center base
    this.SET.baslst[0][1]-=5;
    this.SET.baslst[1][1]+=10;
    this.SET.baslst[2][1]-=5;
  }

  /* get amount of ammo for all bases */
  getBasesTotalAmmoAmount(){
    var bas=document.querySelectorAll('#bases>div:not(.disabled)');
    var amt=0;
    bas.forEach((data)=>{amt+=parseInt(data.innerHTML);});
    return amt;
  }

  /* get nearest base to mouse horizontal x-position */
  getNearestBaseToPos(mx){
    // object list
    var blst=document.querySelectorAll('#bases>.base');
    // search
    var lst=[];
    var key=0;
    blst.forEach((ba)=>{
      if(parseInt(ba.innerHTML)>0){lst=this.TLS.addItemToArray({dist:Math.abs(mx-ba.offsetLeft),key:key},lst);}
      key++;
    });
    // sort by distance ascending
    lst.sort( (x,y)=>{return x.dist-y.dist;} );
    // get object if there is one
    var bobj=null;
    if(lst.length>0){
      bobj=blst[lst[0]['key']];
    }
    // return object
    return bobj;
  }



  /* set cities and bases lists */
  setCitiesAndBasesLists(){
    // basics (3 bases + cities)
    var slts=3+this.SET.ctyamt;
    // create base or city dynamicly
    var px=0;
    var item=[];
    for(var x=0;x<slts;x++){
      if(x%((this.SET.ctyamt/2)+1)==0){
        // pos,ammo,alive
        item=[px,0,1];
        this.TLS.addItemToArray(item,this.SET.baslst);
      }else{
        // pos,0,alive
        item=[px,0,1];
        this.TLS.addItemToArray(item,this.SET.ctylst);
      }
      px+=parseInt(this.SET.winw/slts);
    }
  }



  /* check collisions */
  checkCollisions(){
    // block
    this.SET.blk=true;
    // shots with explosions
    this.checkShotsWithOtherObjectList('expl');
    // shots with ground
    this.checkShotsWithOtherObjectList('grnd');
    // shots with bases
    this.checkShotsWithOtherObjectList('base');
    // shots with cities
    this.checkShotsWithOtherObjectList('cits');
    // unblock
    this.SET.blk=false;
  }

  /* check shots with other object lists */
  checkShotsWithOtherObjectList(which){
    // get object list
    var shts=document.querySelectorAll('#shots>.shot,#shots>.rocket');
    // go check all of them
    shts.forEach((shtobj)=>{
      var skey=shtobj.getAttribute('attr-key');
      var px=shtobj.offsetLeft;
      var py=shtobj.offsetTop;
      // check with which other list
      switch(which){
        case 'expl':
          var expl=document.querySelectorAll('#explos>.explo');
          expl.forEach((explobj)=>{
            if(this.doCollide(shtobj,explobj)){
              this.setShotDeadForKey(skey);
            }
          });
          break;
        case 'grnd':
          var grnd=document.querySelectorAll('#ground');
          grnd.forEach((grndobj)=>{
            if(this.doCollide(shtobj,grndobj)){
              this.TLS.playSound(this.SND.expaud);
              this.addExplosion(px,py,'s');
              this.setShotDeadForKey(skey);
            }
          });
          break;
        case 'base':
          var base=document.querySelectorAll('#bases>.base');
          base.forEach((baseobj)=>{
            var key=baseobj.getAttribute('attr-key');
            if(this.SET.baslst[key][2]==1&&this.doCollide(shtobj,baseobj)){
              this.TLS.playSound(this.SND.expaud);
              this.addExplosion(px,py,'m');
              this.setShotDeadForKey(skey);
              this.SET.baslst[key][1]=0;   // ammo
              this.SET.baslst[key][2]=0;   // alive
            }
          });
          break;
        case 'cits':
          var cits=document.querySelectorAll('#cities>.city');
          cits.forEach((citsobj)=>{
            var key=citsobj.getAttribute('attr-key');
            if(this.SET.ctylst[key][2]==1&&this.doCollide(shtobj,citsobj)){
              this.TLS.playSound(this.SND.expaud);
              this.addExplosion(px,py,'m');
              this.setShotDeadForKey(skey);
              this.SET.ctylst[key][2]=0;   // alive
            }
          });
          break;
      }
    });
  }

  /* do two object collide */
  doCollide(obj1,obj2) {
    if((typeof obj1==='object')&&(typeof obj2==='object')){
      // object bounderies
      var one=obj1.getBoundingClientRect();
      var two=obj2.getBoundingClientRect();
      // check collision
      if(  one.left < two.left   + two.width 
        && one.left + one.width  > two.left 
        && one.top  < two.top    + two.height 
        && one.top  + one.height > two.top) {
          return true;
      }
    }
    return false;
  }



  /* update screen */
  updateScreen(){
    // show objects
    this.drawBases();
    this.drawCities();
    this.drawExplosions();
    this.drawShots();
    // explosion effect
    this.setRandomColorForExplosions();
    // show infos
    this.drawPoints();
    this.drawLevel();
  }



  /* start game */
  startGame(){
    console.log('start game');
    // defaults
    this.SET.shtlst=[];
    this.SET.explst=[];
    this.SET.enmshtamt=this.SET.enmshtamtdef+this.getLevelAddonValue();
    // refill all bases
    this.refillBasesAmmo();
    // update everything
    this.updateScreen();
    // if there are cities and bases left
    if(this.hasPlayerAssets()>0){
      // hide info
      this.TLS.removeClassFromElementById('start','show-elem');
      this.TLS.removeClassFromElementById('pointslist','show-elem');
      // new level or continue game
      if(this.SET.con==false){this.addLevel();}
      // set to default
      this.SET.con=false;
      // start with a little delay
      setTimeout(()=>{
        this.addEnemyShotByTimer();
        this.SET.run=true;
      },1000);
    }
  }

  /* stop game */
  stopGame(){
    console.log('stop game');
    this.SET.run=false;
  }

  /* setup game */
  setupGame(){
    console.log('setup game');
    // cities amount (less on smaller screens)
    this.SET.ctyamt=this.SET.ctyamtdef;
    if(this.SET.winw<=this.SET.mblthr){this.SET.ctyamt=4;}
    // explosion size (less on smaller screens)
    this.SET.exprad=this.SET.expraddef;
    if(this.SET.winw<=this.SET.mblthr){this.SET.exprad*=0.75;}
    // reset player
    this.SET.plrpts=0;
    this.SET.plrlvl=0;
    // defaults
    this.SET.shtlst=[];
    this.SET.baslst=[];
    this.SET.ctylst=[];
    this.SET.explst=[];
    this.SET.enmshtamt=this.SET.enmshtamtdef;
    // set cities and bases
    this.setCitiesAndBasesLists();
    // update everything
    this.updateScreen();
    // no pause
    this.SET.brk=false;
  }

  /* show game info */
  showGameInfo(){
    // hide everything
    this.TLS.removeClassFromElementById('start','show-elem');
    this.TLS.removeClassFromElementById('gameover','show-elem');
    // which info should be visible
    var elmid='start';
    if(this.hasPlayerAssets()==0){elmid='gameover';}
    setTimeout(()=>{this.TLS.addClassToElementById(elmid,'show-elem');},250);
  }

  /* check for win/lose game */
  checkGameStatus(){
    // stop game (no bases or cities left or no enemy shots)
    if(this.SET.run==true&&(this.hasPlayerAssets()==0||(this.SET.enmshtamt==0&&this.SET.shtlst.length==0))){
      this.stopGame();
      if(this.hasPlayerAssets()>0){
        var pts=this.setPointResultList();
        this.TLS.addClassToElementById('pointslist','show-elem');
        this.addPoints(pts);
        setTimeout(()=>{
          this.TLS.removeClassFromElementById('pointslist','show-elem');
          this.drawPoints();
        },10000);
      }
      this.showGameInfo();
    }
  }



  /* init game */
  init(){
    console.log('init');
    // setup basics
    this.SET.winw=window.innerWidth;
    this.SET.winh=window.innerHeight;
    // set game content
    this.setupGame();
    // show gameinfo
    this.showGameInfo();
    // hide cover
    this.TLS.hideCover();
    // start game mover
    this.gameMotor();
    // and go
    console.log('ready!');
  }



  /* start game motor */
  gameMotor(){
    setInterval(()=>{
      // if game is not paused
      if(this.SET.brk==false&&this.SET.run==true){
        this.checkCollisions();
        // if collision check is done
        if(this.SET.blk==false){
          this.SET.shtlst=this.TLS.cleanUpList(this.SET.shtlst,6);
          this.SET.explst=this.TLS.cleanUpList(this.SET.explst,4);
          this.moveShots();
          this.updateScreen();
          this.checkGameStatus();
        }
      }
    },25);
  }



  /* handle listener events */
  handleEvent(event) {
    event.preventDefault();
    switch(event.type) {
      case 'click':
        this.leftClickEvent(event);
        break;
      case 'keydown':
        this.keyEvent(event);
        break;
      case 'resize':
        this.resizeEvent(event);
        break;
    }
  }

  /* left mouse click */
  leftClickEvent(event){
    if(this.SET.run==false){
      if(this.TLS.isElementVisibleById('start')){
        window.dispatchEvent(new KeyboardEvent('keydown',{'code':'KeyS'}));
      }
      if(this.TLS.isElementVisibleById('gameover')){
        window.dispatchEvent(new KeyboardEvent('keydown',{'code':'KeyR'}));
      }
    }else{
      if(this.getBasesTotalAmmoAmount()>0){
        this.TLS.playSound(this.SND.shtaud);
      }else{
        this.TLS.playSound(this.SND.btnaud);
      }
      this.addShotToList('shtlst');
    }
  }

  /* key press event */
  keyEvent(event){
    switch(event.code){
      // s(tart)
      case 'KeyS':
        if(this.SET.run==false){
          this.TLS.playSound(this.SND.hrnaud);
          this.startGame();
        }
        break;
      // r(eset)
      case 'KeyR':
        this.TLS.playSound(this.SND.btnaud);
        this.stopGame();
        this.setupGame();
        this.showGameInfo();
        break;
      // p(ause)
      case 'KeyP':
        this.TLS.playSound(this.SND.btnaud);
        this.SET.brk=!this.SET.brk;
        break;
    }
  }

  /* window resize event */
  resizeEvent(event){
    // stop and restart game
    GME.stopGame();
    GME.init();
  }
}



/* wait until document is fully loaded and ready to start */
let stateCheck=setInterval(()=>{
  if(document.readyState=='complete'){
    clearInterval(stateCheck);
    GME=new Game();
    GME.init();
  }
},250);